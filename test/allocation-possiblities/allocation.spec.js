'use strict';
const expect = require('chai').expect;
const allocationPossibilities = require('../../src/allocation-possibilities');
const peopleService = require('../../src/people').CreatePeopleService();
const groupService = require('../../src/groups').CreateGroupService();
const plannedEvent = require('../../src/planned-event').CreatePlannedEventService();


describe('Allocation possibilities', ()=>{
    it('Finds the possibilities', async ()=>{
        let result = await allocationPossibilities.CreatePossibilities(plannedEvent,peopleService,groupService);
        expect(result.length).equals(5);
        
    });
});