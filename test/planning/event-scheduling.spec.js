'use strict';

const eve = require('../../src/generate-schedule');
const expect = require('chai').expect;
describe('generates event schedule', () => {
    it('calculates the schedule, verified', () => {
        let plan = eve.generateSchedule({
            locale: 'Europe/Berlin',
            days: [3, 5, 6],
            startTime: new eve.Time(9, 30)
        }, Date.UTC(2018, 2, 1, 0, 0, 0), Date.UTC(2018, 2, 8, 0, 0, 0));
        expect(plan.length).equals(3);

        let expected = [
            Date.UTC(2018, 2, 2, 8, 30),
            Date.UTC(2018, 2, 3, 8, 30),
            Date.UTC(2018, 2, 7, 8, 30)
        ];
        let mapped = plan.map(p=>p.getTime());
        expect(mapped).deep.equals(expected);
    });

    it('calculates the schedule', () => {
        let plan = eve.generateSchedule({
            locale: 'Europe/London',
            days: [3, 5, 6],
            startTime: new eve.Time(9, 30)
        }, Date.UTC(2018, 2, 1, 0, 0, 0), Date.UTC(2018, 3, 1, 0, 0, 0));
        expect(plan.length).equals(14);
    });

    it('Calculates block schedules', async () => {
        let schedule1 = {
            id: 'id1',
            locale: 'Europe/London',
            days: [3, 5, 6],
            startTime: new eve.Time(9, 30)
        };
        let schedule2 = {
            id: 'id2',
            locale: 'Europe/Berlin',
            days: [1, 2],
            startTime: new eve.Time(4, 15)
        };

        let plan = await eve.generateBlockSchedule({
            events: [schedule1, schedule2]
        }, Date.UTC(2018, 2, 1, 0, 0, 0), Date.UTC(2018, 3, 1, 0, 0, 0));
        expect(plan.id1.length).equals(14);
        expect(plan.id2.length).equals(8);
    });
});