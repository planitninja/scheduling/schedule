'use strict';
const Time = require('../../src/Time').default;
const expect = require('chai').expect;
const moment = require('moment-timezone');

describe('The time class', ()=>{
    it('Creates a new time', ()=>{
        let time = new Time(9,45);
        expect(time.getValue()).equals(585);
    });

    it('Creates a new time from a moment', ()=>{
        let time = new Time(moment([2017,0,1,9,30]));
        expect(time.getValue()).equals(570);
    });

    it('Creates a moment', ()=>{
        let time = new Time(9,45);
        let moment = time.createDate(2018,1,1);
        expect(moment.toDate().getTime()).equals(Date.UTC(2018,1,1,9,45));
    });

    it('Creates a moment in time zone', ()=>{
        let time = new Time(9,45);
        let moment = time.createDate(2018,1,1,'Europe/Berlin');
        expect(moment.toDate().getTime()).equals(Date.UTC(2018,1,1,8,45));
    });

    it('Creates a moment in time zone, summer time', ()=>{
        let time = new Time(9,45);
        let moment = time.createDate(2018,5,1,'Europe/Berlin');
        expect(moment.toDate().getTime()).equals(Date.UTC(2018,5,1,7,45));
    });

    it('Creates a moment, using moment', ()=>{
        let time = new Time(9,45);
        let date = time.createDate(moment([2018,1,1]));
        expect(date.toDate().getTime()).equals(Date.UTC(2018,1,1,9,45));
    });

    
    it('Creates a moment in timezone, using moment', ()=>{
        let time = new Time(9,45);
        let date = time.createDate(moment([2018,5,1]), 'Europe/Berlin');
        expect(date.toDate().getTime()).equals(Date.UTC(2018,5,1,7,45));
    });

    it('Creates a next day', ()=>{
        let time = new Time(33,45);
        let moment = time.createDate(2018,1,1);
        expect(moment.toDate().getTime()).equals(Date.UTC(2018,1,2,9,45));
    });

    it('Creates a next day, summer time', ()=>{
        let time = new Time(33,45);
        let moment = time.createDate(2018,2,24, 'Europe/Berlin');
        expect(moment.toDate().getTime()).equals(Date.UTC(2018,2,25,8,45));
    });
});