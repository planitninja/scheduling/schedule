import { IGroupService } from '../groups';
import { IPersonService } from '../people';
import { IPlannedEventService, GroupAllocationRule } from '../planned-event';




export async function CreatePossibilities(plannedEventService:IPlannedEventService, peopleService:IPersonService,groupService:IGroupService) {
    let possibilities = [];
    for (let plannedEvent of await plannedEventService.events()) {
        for (let allocationRule of plannedEvent.allocationRules) {
            if (allocationRule.type === 'groupAllocation') {
                let rule = allocationRule as GroupAllocationRule;
                let group = await groupService.group(rule.groupIdentity);
                possibilities.push({id:allocationRule.id, possibilities:group.members});
            }
        }
    }
    return possibilities;
}