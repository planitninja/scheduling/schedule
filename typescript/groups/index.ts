'use strict';
import * as fs from 'fs';

export class Group {
    id: string;
    type: string;
    members: string[];
}

export interface IGroupService {

    group(identity: string): Promise<Group>;
    groups(): Promise<Group[]>;
}

export function CreateGroupService(): IGroupService {

    class temp implements IGroupService {
        
        async group(identity: string): Promise<Group> {
            let groups = await this.groups();
            return groups.filter(g => g.id === identity)[0];
        }
        groups(): Promise<Group[]> {
            return new Promise((accept, reject) => fs.readFile('fake/group.json', (err, data) => {
                if (err) {
                    reject(err);
                }
                let value = data.toString('utf8');
                accept(JSON.parse(value));
            }));
        }
    };
    return new temp();
};