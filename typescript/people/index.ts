'use strict';
import * as fs from 'fs';

export class Person{
    id:string;
}

export interface IPersonService{

    people():Promise<Person[]>;
}

export function CreatePeopleService():IPersonService{

    return {
        people:():Promise<Person[]>=>{
            return new Promise((accept,reject)=>fs.readFile('fake/people.json',(err,data)=>{
                if(err){
                    reject(err);
                }
                let value = data.toString('utf8');
                accept(JSON.parse(value));
            }));
        }
    };
};