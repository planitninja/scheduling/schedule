'use strict';
import * as fs from 'fs';

export interface IEventAllocationRule {
    id: string;
    type: string;
    resourceType: string;
    min: number;
    max: number;
}

export class GroupAllocationRule implements IEventAllocationRule {
    id: string;
    type: string;
    resourceType: string;
    min: number;
    max: number;
    groupIdentity: string;
}

export class PlannedEvent {
    id: string;
    allocationRules: IEventAllocationRule[];
}

export interface IPlannedEventService {
    events(): Promise<PlannedEvent[]>;
}

export function CreatePlannedEventService(): IPlannedEventService {

    return {
        events: (): Promise<PlannedEvent[]> => {
            return new Promise((accept, reject) => fs.readFile('fake/planned-event.json', (err, data) => {
                if (err) {
                    reject(err);
                }
                let value = data.toString('utf8');
                accept(JSON.parse(value));
            }));
        }
    };
};