import * as moment from 'moment-timezone';
import { Moment } from 'moment';

'use strict';


interface ITime {

    getValue(): number;
}

export class Time implements ITime {
    value: number;

    constructor(moment:Moment);
    constructor(hour: number, minute: number);
    constructor(hourOrMoment: any, minute?: number) {
        if (hourOrMoment instanceof moment) {
           this.value = (hourOrMoment.hour() * 60) + hourOrMoment.minute();
        }
        else {
            hourOrMoment = hourOrMoment || 0;
            minute = minute || 0;
            this.value = (hourOrMoment * 60) + minute;
        }

        
    }
    createDate(input: Moment, timezone?:string): Moment;
    createDate(year: number, month: number, date: number, timezone?:string): Moment;
    createDate(yearOrMoment: any, monthOrTimeZone: any, date?: number, timezone?:string): Moment {
        let val = [];
        if (yearOrMoment instanceof moment) {
            timezone = monthOrTimeZone;
           val = [yearOrMoment.year(),yearOrMoment.month(),yearOrMoment.date()];
        }
        else {
            val=[yearOrMoment, monthOrTimeZone, date];
        }
        return moment.tz(val,timezone).add('minutes', this.value);
    }

    getValue() {
        return this.value;
    }
}

export default Time;