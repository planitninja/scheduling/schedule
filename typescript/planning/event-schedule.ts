import Time from './Time';


export interface IEventBlockSchedule{
    id:string;
    events:IEventSchedule[];
}

export interface IEventSchedule {
    id:string,
    locale: string,
    days: number[],
    startTime: Time,
    duration: number
}

