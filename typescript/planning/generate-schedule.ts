'use strict';
import { IEventSchedule, IEventBlockSchedule } from './event-schedule';
import Time from './Time';
import * as moment from 'moment-timezone';


function daysToMap(days: number[]) {
    if (days) {
        let map = 0;
        for (let day of days) {
            map = map | (1 << (day + 1));
        }
        return map;
    }
    return 0;
}

function hasDay(day: number, map: number) {
    return ((1 << day + 1) & map) > 0;
}


export async function generateBlockSchedule(eventBlockSchedule: IEventBlockSchedule, startDate: Date, endDate: Date) {
    let scheduleRunner = (eventSchedule: IEventSchedule):Promise<{id:string,schedule:Date[]}> => {
        return new Promise((accept,reject)=>{
            let schedule = generateSchedule(eventSchedule, startDate, endDate);
            accept({ id: eventSchedule.id, schedule });
        });
        
    }
    let tasks = eventBlockSchedule.events.map(i => scheduleRunner(i));
    let results = await Promise.all(tasks);
    return results.reduce((m:any,c:{id:string,schedule:Date[]})=>{
        m[c.id] = c.schedule;
        return m;
    },{});
}

export function generateSchedule(eventSchedule: IEventSchedule, startDate: Date, endDate: Date): Date[] {
    let localStart = moment(startDate).tz(eventSchedule.locale);
    let localEnd = moment(endDate).tz(eventSchedule.locale);
    let windowStartTime = new Time(localStart);
    let schedule: Date[] = [];
    let mappedDays = daysToMap(eventSchedule.days);

    while (localStart < localEnd) {
        if (hasDay(localStart.day(), mappedDays)) {
            let scheduleDate = eventSchedule.startTime.createDate(localStart, eventSchedule.locale);
            schedule.push(scheduleDate.utc().toDate());
        }
        localStart = localStart.add(1, 'days');
    }
    return schedule;
}

export { Time };